package com.avanxo.transformers;

public class XMLCleaner {
	public static String cleanXML(String xml) {
		return xml.replaceAll("&#0;", "0").replaceAll("\\\n", "");
	}
}
